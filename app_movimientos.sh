cd /home/alumno/repobitbucket/proyectotechucsm/backend
sudo docker build -t app-back-nodejs .

cd /home/alumno/repobitbucket/mogondocker
sudo docker build -t app-bbdd-mongo .

cd /home/alumno/repobitbucket/postgredocker
sudo docker build -t app-bbdd-postgres .

cd /home/alumno/repobitbucket/proyectotechucsm
sudo docker kill servertechu
sudo docker rm servertechu
sudo docker kill servermongo
sudo docker rm servermongo
sudo docker kill serverpostgres
sudo docker rm serverpostgres

sudo docker run -v /opt/datosmongo:/data/db -p 27017:27017 --name servermongo --net redtechu -d app-bbdd-mongo
sudo docker run -v /var/lib/postgres/data:/var/lib/postgresql/data -p 5433:5432 --name serverpostgres --net redtechu -d app-bbdd-postgres
sudo docker run -p 3001:3000 --name servertechu --net redtechu -d app-back-nodejs

#sudo docker kill clienttechu
#sudo docker rm clienttechu
#cd /home/alumno/.nvm/versions/node/v6.0.0/bin/polymer
#polymer build --root "/home/alumno/repobitbucket/proyectotechucsm/front"
#cd /home/alumno/repobitbucket/proyectotechucsm/front
#sudo docker build -t app-front-polymer .
#sudo docker run -p 5433:5432 --name serverpostgresql --net redtechu -d app-bbdd-postgresql
#sudo docker run -v /etc/postgresql:/etc/postgresql -v /var/log/postgresql:/var/log/postgresql -v /var/lib/postgresql:/var/lib/postgresql -p 5433:5432 --name serverpostgresql --net redtechu -d app-bbdd-postgresql
#sudo docker run -v /var/lib/postgresql/data:/var/lib/postgresql/data -p 5433:5432 --name serverpostgresql_2 --net redtechu -d app-bbdd-postgresql_2
#sudo docker run -p 9080:3000 --name clienttechu --net redtechu -d app-front-polymer # levanta contendor POLYMER
#cd /home/alumno/repobitbucket/proyectotechucsm/frontend
#firefox http://localhost:9080/ .


function sendBackServer(method, url, params, callback) {
  var urlServidor = "http://localhost:3001/";
  var http = new XMLHttpRequest();
  http.open(method, urlServidor+url, true);
  http.responseType = "application/json"
  http.setRequestHeader("Content-type", "application/json; charset=utf-8");
  if(method=='GET'){
    http.send();
  }else{
    http.send(JSON.stringify(params));
  }
  http.onreadystatechange = function() {
    if(http.readyState == 4 && http.status == 200) {
      var resJSON = JSON.parse(http.responseText);
      return callback (resJSON);
    }
  }
}

function ExportToExcel(myItems){
  alasql("SELECT * INTO XLSX('cities.xlsx',{headers:true}) FROM ? ",[myItems]);
}

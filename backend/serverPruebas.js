var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var cors = require('cors');

var path = require('path');

//Variable para el request json que voy a utilizar
var bodyparser = require('body-parser');

//Variable para el request json que voy a utilizar
var requestjson = require('request-json');

//URL de mi colleción de movimientos en mlab
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos";
var api_Key = "apiKey=RTROqHkuqvONDqFwwBltsF5nikcslEik";
var clientMlab = requestjson.createClient(urlmovimientosMlab + "?" + api_Key);

//Conexión mongodb
var mongoClient = require('mongodb').MongoClient;
var urlServidorMongo = "mongodb://servermongo:27017/local";

//Conexión postgreSQL
var pg = require('pg');
var urlServidorPostgre = "postgres://docker:docker@localhost:5433/bdusuarios";
var clientPostgre = new pg.Client(urlServidorPostgre);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.use(bodyparser.json());

app.use(cors());

/**
  * Verificación login
  * @param JSON: usuario, password
  * @response JSON: respuesta
  *	  0-validación correcta
  *	  1-Usuario no existe
  *	  2-Password incorrecta
  *
**/
app.post('/login', function(req, res){
  console.log(req.body);
  var obj = req.body;
  var out;
  if(obj.usuario==obj.password){
    out=0;
  }else if(obj.usuario=="1"){
    out=1;
  }else if(obj.password=="2"){
    out=2;
  }
  res.setHeader('Content-Type', 'application/json');
  var myJSON = { "respuesta": out };
  res.send(JSON.stringify(myJSON));
});

app.post('/loginpostgreSQL', function(req, res){
  console.log('AQUIIIII');

  //Crear clioente postgreedocker
  clientPostgre.connect();
  //Hacer consulta. Asumo recibo req.body = {usuario:xxxxx, password:yyyyy}
  const query = clientPostgre.query('SELECT COUNT (*) FROM usuarios WHERE usuario = $1 AND password = $2', [req.body.usuario, req.body.password], (err, result) => {
    if(err){
      console.log(err);
      res.send(err);
    }else{
      if(result.rows[0].count >= 1){
        res.send('login correcto');
      }else {
        res.send('login incorrecto');
      }
      console.log(result.rows[0]);
    }
  });
  //Devolver resultado
});

app.post('/insertusuario', function(req, res){
  //Crear clioente postgreedocker
  clientPostgre.connect();

  const query = clientPostgre.query('INSERT INTO usuarios (usuario, password) VALUES ($1, $2)', [req.body.usuario, req.body.password], (err, result) => {
    if(err){
      console.log(err);
      res.send(err);
    }else{
      console.log(result.rows[0]);
      res.send(result.rows[0]);
    }
  });
});

app.get('/movimientosdbmongo', function(req, res){
  mongoClient.connect(urlServidorMongo, function(err, db){
    if(err){
      console.log(err);
    }else{
      var col = db.collection('movimientos');
      console.log('conexión a mongo realizada correctamente');
      col.find({}).limit(3).toArray(function(err, docs){
        res.send(docs);
      });
      db.close();
    }
  })
});

app.post('/movimientosdbmongo', function(req, res){
  mongoClient.connect(urlServidorMongo, function(err, db){
    if(err){
      console.log(err);
    }else{
      var col = db.collection('movimientos');
      console.log('conexión a mongo realizada correctamente');
      col.insertOne(req.body,function(err, docs){
        console.log('insertado correctamente');
      });
      db.close();
    }
  })
});

app.post('/movimientos/bodypolymer', function(req, res){
  console.log(req.body);
  var myJSON = req.body;
  //var myJSON = JSON.parse (req.body);
  console.log("inicio");
  //Ataco a la API de MLAB con request-json
  //Creo el cliente apuntando a la API MLAD
  clientMlab.post('', myJSON, function(err, resM, body) { //Esto si paso el JSON desde el body de postman
    if(err){
      //Si hay error saco por consola el error
      console.log("Error en MLAB : "+body);
    } else {
      res.send(resM.body);
      //res.send(JSON.stringify(JSON.parse(resM)));
      //res.send(JSON.stringify(resM));
      console.log(body);
    }
  });
});

app.get('/movimientos', function(req, res){
  //Ataco a la API de MLAB con request-json
  //Creo el cliente apuntando a la API MLAD

  clientMlab.get('', function(err, resM, body) {
    if(err){
      //Si hay error saco por consola el error
      console.log(body);
    } else {
      res.send(body);
    }
  });
});

app.post('/movimientos', function(req, res){
  console.log("inicio");
  //Ataco a la API de MLAB con request-json
  //Creo el cliente apuntando a la API MLAD
  var myJSON = { "idcliente": 32325, "nombre": "Carlos5", "apellido": "Sastre5" };
  clientMlab.post('', myJSON, function(err, resM, body) {  //Esto si paso el JSON por código
    if(err){
      //Si hay error saco por consola el error
      console.log("Error: "+body);
    } else {
      console.log(body);
    }
  });
});

app.post('/movimientos/body', function(req, res){
  console.log("inicio");
  //Ataco a la API de MLAB con request-json
  //Creo el cliente apuntando a la API MLAD
  clientMlab.post('', req.body, function(err, resM, body) { //Esto si paso el JSON desde el body de postman
    if(err){
      //Si hay error saco por consola el error
      console.log("Error: "+body);
    } else {
      console.log(body);
    }
  });
});

app.put('/movimientos', function(req, res){
  console.log("inicio");
  //Ataco a la API de MLAB con request-json
  //Creo el cliente apuntando a la API MLAD

  clientMlab = requestjson.createClient(urlmovimientosMlab+"?q={'idcliente':32324}"+"&"+api_Key);
  clientMlab.put('', req.body, function(err, resM, body) { //Esto si paso el JSON desde el body de postman
    if(err){
      //Si hay error saco por consola el error
      console.log("Error: "+body);
    } else {
      console.log(body);
    }
  });
});

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//Variable para el request json que voy a utilizar
var bodyparser = require('body-parser');

//Variable para el request json que voy a utilizar
var requestjson = require('request-json');

//URL de mi colleción de movimientos en mlab
var urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=RTROqHkuqvONDqFwwBltsF5nikcslEik";

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.use(bodyparser.json());
// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*'); //Al ser app local se permite acceso completo: En app real se controlarían la URLs que invocan al servidor
    res.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE"); //Todos los métodos accesibles
    res.setHeader("Access-Control-Allow-Headers", "Content-type");
    res.setHeader("Access-Control-Request-Headers", "Content-type, application/json");
    // Pass to next layer of middleware
    next();
});

app.get('/movimientos', function(req, res){
  //Ataco a la API de MLAB con request-json
  //Creo el cliente apuntando a la API MLAD
  var clientMlab = requestjson.createClient(urlmovimientosMlab);
  clientMlab.get('', function(err, resM, body) {
    if(err){
      //Si hay error saco por consola el error
      console.log(body);
    } else {
      res.send(body);
    }
  });
});

app.post('/movimientos', function(req, res){
  console.log("inicio");
  //Ataco a la API de MLAB con request-json
  //Creo el cliente apuntando a la API MLAD
  var clientMlab = requestjson.createClient(urlmovimientosMlab);
  var myJSON = { "idcliente": 32325, "nombre": "Carlos5", "apellido": "Sastre5" };
  clientMlab.post('', myJSON, function(err, resM, body) {  //Esto si paso el JSON por código
    if(err){
      //Si hay error saco por consola el error
      console.log("Error: "+body);
    } else {
      console.log(body);
    }
  });
});

app.post('/movimientos/body', function(req, res){
  console.log("inicio");
  //Ataco a la API de MLAB con request-json
  //Creo el cliente apuntando a la API MLAD
  var clientMlab = requestjson.createClient(urlmovimientosMlab);
  clientMlab.post('', req.body, function(err, resM, body) { //Esto si paso el JSON desde el body de postman
    if(err){
      //Si hay error saco por consola el error
      console.log("Error: "+body);
    } else {
      console.log(body);
    }
  });
});

app.post('/movimientos/bodypolymer', function(req, res){
  var myJSON = JSON.parse (req.params.JSONparam);
  console.log("inicio");
  //Ataco a la API de MLAB con request-json
  //Creo el cliente apuntando a la API MLAD
  var clientMlab = requestjson.createClient(urlmovimientosMlab);
  clientMlab.post('', myJSON, function(err, resM, body) { //Esto si paso el JSON desde el body de postman
    if(err){
      //Si hay error saco por consola el error
      console.log("Error: "+body);
    } else {
      console.log(body);
    }
  });
});


app.put('/movimientos', function(req, res){
  console.log("inicio");
  //Ataco a la API de MLAB con request-json
  //Creo el cliente apuntando a la API MLAD
  //urlmovimientosMlab = "https://api.mlab.com/api/1/databases/bdbanca/collections/movimientos?apiKey=RTROqHkuqvONDqFwwBltsF5nikcslEik";
  var clientMlab = requestjson.createClient(urlmovimientosMlab);
  clientMlab.put('&q={"idcliente":32324}', req.body, function(err, resM, body) { //Esto si paso el JSON desde el body de postman
    if(err){
      //Si hay error saco por consola el error
      console.log("Error: "+body);
    } else {
      console.log(body);
    }
  });
});

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var cors = require('cors');

var path = require('path');

//Variable para el request json que voy a utilizar
var bodyparser = require('body-parser');

//Variable para el request json que voy a utilizar
var requestjson = require('request-json');

//Conexión mongodb
var mongoClient = require('mongodb').MongoClient;
var urlServidorMongo = "mongodb://servermongo:27017/local";

//Conexión postgreSQL
var pg = require('pg');
var urlServidorPostgre = "postgres://postgres:postgres@serverpostgres:5432/dbusuarios";

//API externa
var urlAPIQuandl = "https://www.quandl.com/api/v3/datasets/WIKI/TICKET/data.json";
var api_Key = "api_key=8v4tVDy65xr_uPF1ZSNu";

var clientPostgre = new pg.Client(urlServidorPostgre);

var enigma = require('enigma-code');
var valorEncriptacion = 10;
var key = 'd6F3Efeq';


// pruebas postman
//var urlServidorPostgre = "postgres://docker:docker@localhost:5433/dbusuarios";
//var urlServidorMongo = "mongodb://localhost:27017/local";

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.use(bodyparser.json());

app.use(cors());

/**
  * Verificación login contra postgresql, Valida primero el usuario y después la password
  * @param JSON: usuario, password
  * @response text
  * OK validación correcta
  * NOKUSER usuario ya existe
  * NOKPASSWORD password incorrecta
  *
**/
app.post('/login', function(req, res){
  res.setHeader('Content-Type', 'application/json');
  var resJSON;
  clientPostgre.connect();
  //Hacer consulta para saber si existe el usuario.
  const query = clientPostgre.query('SELECT COUNT (*) FROM users WHERE usuario = $1', [req.body.usuario], (err, result) => {
    if(err){
      console.log(err);
      resJSON = { "respuesta": err };
      res.send(JSON.stringify(resJSON));
    }else{
      if(result.rows[0].count < 1){
        resJSON = { "respuesta": "NOKUSER" };
        res.send(JSON.stringify(resJSON));
      } else {
        //Hacer consulta para saber password es correcta.
        var passwordEncrypted = enigma.genHash(valorEncriptacion,key,req.body.password,function(err,hash){
          if(err) return console.log(err);
          return hash;
        });
        const query2 = clientPostgre.query('SELECT COUNT (*) FROM users WHERE usuario = $1 AND password = $2', [req.body.usuario, passwordEncrypted], (err, result) => {
          if(err){
            console.log(err);
            resJSON = { "respuesta": err };
            res.send(JSON.stringify(resJSON));
          }else{
            if(result.rows[0].count < 1){
              resJSON = { "respuesta": "NOKPASSWORD" };
              res.send(JSON.stringify(resJSON));
            }else {
              const query3 = clientPostgre.query('SELECT usuario, nombre, mail, fecha_alta FROM users WHERE usuario = $1', [req.body.usuario], (err, result) => {
                if(err){
                  console.log(err);
                  resJSON = { "respuesta": err };
                  res.send(JSON.stringify(resJSON));
                }else{
                  resJSON = { "respuesta": "OK", "usuariologado": result.rows[0]};
                  res.send(JSON.stringify(resJSON));
                }
              });
            }
          }
        });
      }
    }
  });
});


/**
  * Creación nuevo usuario
  * @param JSON: usuario, password, nombre, emial
  * @response text
  * OK usuario insertado
  * NOK usuario ya existe
  *
**/
app.post('/signup', function(req, res){
  res.setHeader('Content-Type', 'application/json');
  var resJSON;
  //Crear clioente postgreedocker
  clientPostgre.connect();
  //Hacer consulta para comprobar que el usuario no está ya insertedo.
  const query = clientPostgre.query('SELECT COUNT (*) FROM users WHERE usuario = $1', [req.body.usuario], (err, result) => {
    if(err){
      console.log(err);
      resJSON = { "respuesta": err };
      res.send(JSON.stringify(resJSON));
    }else{
      if(result.rows[0].count > 0){
        resJSON = { "respuesta": "NOK" };
        res.send(JSON.stringify(resJSON));
      }else {
        var passwordEncrypted = enigma.genHash(valorEncriptacion,key,req.body.password,function(err,hash){
          if(err) return console.log(err);
          return hash;
        });
        const query2 = clientPostgre.query('INSERT INTO users (usuario, password, nombre, mail, fecha_alta) VALUES ($1, $2, $3, $4, CURRENT_DATE)', [req.body.usuario, passwordEncrypted, req.body.nombre, req.body.mail], (err, result) => {
          if(err){
            console.log(err);
            resJSON = { "respuesta": err };
            res.send(JSON.stringify(resJSON));
          }else{
            const query3 = clientPostgre.query('SELECT usuario, nombre, mail, fecha_alta FROM users WHERE usuario = $1', [req.body.usuario], (err, result) => {
              if(err){
                console.log(err);
                resJSON = { "respuesta": err };
                res.send(JSON.stringify(resJSON));
              }else{
                resJSON = { "respuesta": "OK", "usuariologado": result.rows[0]};
                res.send(JSON.stringify(resJSON));
              }
            });
          }
        });
      }
    }
  });
});

/**
  * Ontención movimiento del un usuario
  * @param JSON: usuario Logado
  * @response text colección de movimientos del usuario
  *
**/
app.get('/movimientos/:usuario', function(req, res){
  res.setHeader('Content-Type', 'application/json');
  var resJSON;
  var usuarioLogado = req.params.usuario;
  var accion = req.query.accion;
  var ticket = req.query.ticket;
  var tipoOperacion = req.query.tipoOperacion;
  var fechaOperacion = req.query.fechaOperacion;
  var tributado = req.query.tributado;

  //Construimos los criterios de consulta
  var criteriosJSON={};
  criteriosJSON["usuario"]=usuarioLogado;
  if(accion!='' && accion!=undefined){
    criteriosJSON["accion"]=accion;
  }
  if(ticket!='' && ticket!=undefined){
    criteriosJSON["ticket"]=ticket;
  }
  if(tipoOperacion!='' && tipoOperacion!=undefined){
    criteriosJSON["tipoOperacion"]=tipoOperacion;
  }
  if(fechaOperacion!='' && fechaOperacion!=undefined){
    criteriosJSON["fechaOperacion"]=fechaOperacion;
  }
  if(tributado!='' && tributado!=undefined){
    criteriosJSON["tributado"]=tributado;
  }

  mongoClient.connect(urlServidorMongo, function(err, db){
    if(err){
      console.log(err);
      resJSON = { "respuesta": "NOK" };
      res.send(JSON.stringify(resJSON));
    }else{
      var col = db.collection('movimientos');
      console.log('conexión a DB mongo realizada correctamente!');
      col.find(criteriosJSON).limit(20).toArray(function(err, docs){
        resJSON = { "respuesta": docs };
        res.send(JSON.stringify(resJSON));
      });
      db.close();
    }
  })
});

/**
  * Inserción de un movimiento
  * @param JSON: los datros del movimiento
  * OK movmiento insertdo correctamente
  * NOK error en el proceso de inserción
  *
**/
app.put('/movimientos', function(req, res){
  res.setHeader('Content-Type', 'application/json');
  var resJSON;
  var movimiento = req.body;
  mongoClient.connect(urlServidorMongo, function(err, db){
    if(err){
      console.log(err);
      resJSON = { "respuesta": "NOK" };
      res.send(JSON.stringify(resJSON));
    }else{
      var col = db.collection('movimientos');
      console.log('conexión a mongo realizada correctamente');
      col.insertOne(movimiento);
      resJSON = { "respuesta": "OK" };
      res.send(JSON.stringify(resJSON));
      db.close();
    }
  })
});

/**
  * Ontención cotizaciones de un valor de del DOW JONES. Se obtine de API Externa quandl.com
  * @param JSON: empresa del DOW JONES (TICKET)
  * @response JSON de cotizaciones de la empresa
  *
**/
app.get('/cotizaciones/:ticket', function(req, res){
  res.setHeader('Content-Type', 'application/json');
  var ticket = req.params.ticket;
  var resJSON;
  var url = urlAPIQuandl.replace("TICKET", ticket)+ "?limit=20&" + api_Key;
  console.log(url);

  var clientQuandl = requestjson.createClient(url);
  clientQuandl.get('', function(err, resM, body) {
    if(err){
      //Si hay error saco por consola el error
      resJSON = { "respuesta": "NOK" };
      res.send(JSON.stringify(resJSON));
    } else {
      resJSON = { "respuesta": body };
      res.send(JSON.stringify(resJSON));
    }
  });
});

app.get('/pruebaencryptacion', function(req, res){
    var prueba = enigma.genHash(valorEncriptacion,key,'contraseña123',function(err,hash){
      if(err) return console.log(err);
      return hash;
    });
    console.log(prueba);
    var prueba2 = enigma.Desencriptar(prueba,function(err,des){
      if(err) return console.log(err);
      return des;
    });
    console.log(prueba2);
});
